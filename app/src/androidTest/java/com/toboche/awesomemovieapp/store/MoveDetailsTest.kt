package com.toboche.awesomemovieapp.store

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import android.view.View
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.movieslist.MovieActivity
import org.hamcrest.CoreMatchers.`is`
import org.junit.Rule
import org.junit.Test

class MoveDetailsTest {

    @get:Rule
    val activityRule = ActivityTestRule(MovieActivity::class.java)

    @Test
    fun shouldShowMovieDetails() {
        onView(withText("Top Gun 1"))
            .perform(click())

        onView(withText("Score: 6.0"))
            .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        onView(withText("As students at the United States Navy's elite fighter weapons school compete to be best in the class, one daring young pilot learns a few things from a civilian instructor that are not taught in the classroom."))
            .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        onView(withId(R.id.movie_actors))
            .check(RecyclerViewItemCountAssertion(3))
    }

    class RecyclerViewItemCountAssertion(val expectedCount: Int) : ViewAssertion {


        override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
            if (noViewFoundException != null) {
                throw noViewFoundException
            }

            val recyclerView = view as RecyclerView
            assertThat(recyclerView.adapter!!.itemCount, `is`(expectedCount))
        }
    }

}
