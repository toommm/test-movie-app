package com.toboche.awesomemovieapp.store

import com.toboche.awesomemovieapp.model.Actor
import com.toboche.awesomemovieapp.model.MovieDetail
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MoviesStoreTest {

    val systemUnderTest = MoviesStore()

    @Test
    fun shouldLoadMoviesList() {
        val actualMovies = systemUnderTest.getMovies()

        assertThat(actualMovies).hasSize(10)
        assertThat(actualMovies[0].name).isEqualTo("Top Gun 0")
        assertThat(actualMovies[1].name).isEqualTo("Top Gun 1")
    }

    @Test
    fun shouldGetMovieDetails() {
        val actualMovieDetail = systemUnderTest.getMovieDetail("Top Gun 1")

        assertThat(actualMovieDetail).isEqualTo(
            MovieDetail(
                "Top Gun 1",
                9.0F,
                arrayOf(
                    Actor(
                        name = "Tom Cruise",
                        age = 50,
                        imageUrl = ""
                    ),
                    Actor(
                        name = "Val Kilmer",
                        age = 46,
                        imageUrl = "https://m.media-amazon.com/images/M/MV5BMTk3ODIzMDA5Ml5BMl5BanBnXkFtZTcwNDY0NTU4Ng@@._V1_UY317_CR4,0,214,317_AL_.jpg"
                    ),
                    Actor(
                        name = "Jennifer Connelly",
                        age = 39,
                        imageUrl = "https://m.media-amazon.com/images/M/MV5BOTczNTgzODYyMF5BMl5BanBnXkFtZTcwNjk4ODk4Mw@@._V1_UY317_CR12,0,214,317_AL_.jpg"
                    )
                ),
                "As students at the United States Navy's elite fighter weapons school compete to be best in the class, one daring young pilot learns a few things from a civilian instructor that are not taught in the classroom."
            )
        )
    }
}