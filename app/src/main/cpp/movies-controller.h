//
// Created by Tomasz on 12/01/2019.
//

#ifndef AWESOMEMOVIEAPP_MOVIES_CONTROLLER_H
#define AWESOMEMOVIEAPP_MOVIES_CONTROLLER_H

#include <string>
#include <vector>
#include <map>

namespace movies {
    class Actor {
    public:
        std::string name;
        int age;

        //optional challenge 1: Load image from URL
        std::string imageUrl;
    };


    class Movie {
    public:
        std::string name;
        int lastUpdated;

    };

    class MovieDetail {
    public:
        std::string name;
        float score;
        std::vector<Actor> actors;
        std::string description;
    };

    class MovieController {
    private:
        std::vector<Movie *> _movies;
        std::map<std::string, MovieDetail *> _details;

    public:
        MovieController();

        std::vector<Movie *> getMovies();

        MovieDetail *getMovieDetail(std::string name);
    };
}

#endif //AWESOMEMOVIEAPP_MOVIES_CONTROLLER_H
