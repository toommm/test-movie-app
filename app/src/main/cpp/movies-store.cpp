//
// Created by Tomasz on 12/01/2019.
//

#include <jni.h>
#include "movies-controller.h"
#include "movies-controller.cpp"

using namespace movies;

//consider clearing this class
static MovieController movieController;

jobject mapToMovie(JNIEnv *env, jclass movieClass, jmethodID constructor, const Movie *movieToPass);

jobject mapToActor(JNIEnv *env, jclass actorClass, jmethodID actorConstructor, const Actor &actorToMap);

jobjectArray mapToActors(JNIEnv *env, const MovieDetail *movieDetail);

extern "C" JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
//__android_log_print(ANDROID_LOG_INFO,  __FUNCTION__, "onLoad");
    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    movieController = MovieController();
    return JNI_VERSION_1_6;
}

extern "C" JNIEXPORT jobjectArray JNICALL
Java_com_toboche_awesomemovieapp_store_MoviesStore_getMovies(
        JNIEnv *env,
        jobject /* this */) {
    const std::vector<Movie *> &movies = movieController.getMovies();

    jclass movieClass = env->FindClass("com/toboche/awesomemovieapp/model/Movie");
    jmethodID constructor = env->GetMethodID(movieClass, "<init>", "(Ljava/lang/String;I)V");

    const std::vector<Movie *> &moviesToPass = movieController.getMovies();
    jobjectArray returnedMoviesArray = env->NewObjectArray(moviesToPass.size(), movieClass, 0);

    for (std::size_t i = 0, e = moviesToPass.size(); i != e; ++i) {
        Movie *movieToPass = moviesToPass[i];
        jobject movieObjectToReturn = mapToMovie(env, movieClass, constructor, movieToPass);
        env->SetObjectArrayElement(returnedMoviesArray, i, movieObjectToReturn);
    }

    return returnedMoviesArray;
}

jobject mapToMovie(JNIEnv *env, jclass movieClass, jmethodID constructor, const Movie *movieToPass) {
    std::__ndk1::string name = movieToPass->name;
    jstring nameJString = env->NewStringUTF(name.c_str());
    jobject movieObjectToReturn = env->NewObject(movieClass, constructor, nameJString, 1);
    return movieObjectToReturn;
}

extern "C" JNIEXPORT jobject JNICALL
Java_com_toboche_awesomemovieapp_store_MoviesStore_getMovieDetail(
        JNIEnv *env,
        jobject /* this */,
        jstring nameJString) {
    const char *name = env->GetStringUTFChars(nameJString, NULL);
    MovieDetail *movieDetail = movieController.getMovieDetail(name);
    jstring nameToReturn = env->NewStringUTF(movieDetail->name.c_str());
    jfloat scoreToReturn = movieDetail->score;

    jobjectArray actorsToReturn = mapToActors(env, movieDetail);
    jstring descriptionToReturn = env->NewStringUTF(movieDetail->description.c_str());

    jclass movieDetailClass = env->FindClass("com/toboche/awesomemovieapp/model/MovieDetail");
    jmethodID constructor = env->GetMethodID(movieDetailClass, "<init>",
                                             "(Ljava/lang/String;F[Lcom/toboche/awesomemovieapp/model/Actor;Ljava/lang/String;)V");

    jobject movieDetailsToReturn = env->NewObject(movieDetailClass, constructor,
                                                  nameToReturn, scoreToReturn, actorsToReturn, descriptionToReturn);

    return movieDetailsToReturn;
}

jobjectArray mapToActors(JNIEnv *env, const MovieDetail *movieDetail) {
    jclass actorClass = env->FindClass("com/toboche/awesomemovieapp/model/Actor");
    jmethodID actorConstructor = env->GetMethodID(actorClass, "<init>", "(Ljava/lang/String;ILjava/lang/String;)V");

    const std::__ndk1::vector<Actor> &actors = movieDetail->actors;
    jobjectArray actorsToReturn = env->NewObjectArray(actors.size(), actorClass, 0);
    for (size_t i = 0, e = actors.size(); i != e; ++i) {
        const Actor &actorToMap = actors[i];
        jobject actorToReturn = mapToActor(env, actorClass, actorConstructor, actorToMap);
        env->SetObjectArrayElement(actorsToReturn, i, actorToReturn);
    }
    return actorsToReturn;
}

jobject mapToActor(JNIEnv *env, jclass actorClass, jmethodID actorConstructor, const Actor &actorToMap) {
    jstring actorNameToReturn = env->NewStringUTF(actorToMap.name.c_str());
    jstring actorImageUrlToReturn = env->NewStringUTF(actorToMap.imageUrl.c_str());
    int actorAgeToReturn = actorToMap.age;
    jobject actorToReturn = env->NewObject(actorClass, actorConstructor, actorNameToReturn, actorAgeToReturn,
                                           actorImageUrlToReturn);
    return actorToReturn;
}

