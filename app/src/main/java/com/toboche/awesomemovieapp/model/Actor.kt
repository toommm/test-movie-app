package com.toboche.awesomemovieapp.model

data class Actor(
    val name: String,
    val age: Int,
    val imageUrl: String
)