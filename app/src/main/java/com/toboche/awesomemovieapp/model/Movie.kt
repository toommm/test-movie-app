package com.toboche.awesomemovieapp.model

data class Movie(
    val name: String,
    val lastUpdated: Int
)