package com.toboche.awesomemovieapp.model

data class MovieDetail(
    val name: String,
    val score: Float,
    val actors: Array<Actor>,
    val description: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieDetail

        if (name != other.name) return false
        if (score != other.score) return false
        if (!actors.contentEquals(other.actors)) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + score.hashCode()
        result = 31 * result + actors.contentHashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}