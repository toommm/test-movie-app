package com.toboche.awesomemovieapp.moviedetails

data class ActorListItem(
    val name: String,
    val age: String,
    val imageUrl: String
)