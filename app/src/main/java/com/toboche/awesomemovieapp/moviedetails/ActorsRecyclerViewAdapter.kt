package com.toboche.awesomemovieapp.moviedetails

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.toboche.awesomemovieapp.R
import kotlinx.android.synthetic.main.actor_list_content.view.*

class ActorsRecyclerViewAdapter :
    RecyclerView.Adapter<ActorsRecyclerViewAdapter.ActorViewHolder>() {

    private var values: List<ActorListItem> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActorViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.actor_list_content, parent, false)
        return ActorViewHolder(view)
    }

    override fun onBindViewHolder(holderActor: ActorViewHolder, position: Int) {
        val item = values[position]
        holderActor.name.text = item.name
        holderActor.age.text = item.age
        if (item.imageUrl.isEmpty()) {
            return
        }
        Picasso.get().load(item.imageUrl)
            .placeholder(R.drawable.ic_launcher_background)
            .into(holderActor.image)
    }

    override fun getItemCount() = values.size

    fun setActors(actors: List<ActorListItem>) {
        values = actors
        notifyDataSetChanged()
    }

    inner class ActorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.actor_name
        val age: TextView = view.actor_age
        val image: ImageView = view.actor_image
    }
}