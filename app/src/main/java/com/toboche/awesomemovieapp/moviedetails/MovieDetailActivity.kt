package com.toboche.awesomemovieapp.moviedetails

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.movieslist.MovieActivity
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        setSupportActionBar(detail_toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = MovieDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(
                        MovieDetailFragment.MOVIE_NAME_ARGUMENT_ID,
                        intent.getStringExtra(MovieDetailFragment.MOVIE_NAME_ARGUMENT_ID)
                    )
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.movie_detail_container, fragment)
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                navigateUpTo(Intent(this, MovieActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
