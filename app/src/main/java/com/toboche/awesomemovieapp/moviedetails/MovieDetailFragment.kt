package com.toboche.awesomemovieapp.moviedetails

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.store.MoviesStore
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.movie_detail.*

class MovieDetailFragment : Fragment(), MovieDetailsView {

    private lateinit var movieDetailsPresenter: MovieDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        movieDetailsPresenter = MovieDetailsPresenter(
            MoviesStore(),
            arguments!!.getString(MOVIE_NAME_ARGUMENT_ID)!!,
            context!!
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.movie_detail, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (movie_actors.layoutManager as GridLayoutManager).spanCount =
                context!!.resources.getInteger(R.integer.actors_list_span_count)
        movie_actors.adapter = ActorsRecyclerViewAdapter()

        movieDetailsPresenter.attach(this)
    }

    override fun onDestroyView() {
        movieDetailsPresenter.detach()
        super.onDestroyView()
    }

    override fun setActors(actors: List<ActorListItem>) {
        (movie_actors.adapter as ActorsRecyclerViewAdapter).setActors(actors)
    }

    override fun setScore(score: String) {
        movie_score.text = score
    }

    override fun setDescription(description: String) {
        movie_description.text = description
    }

    override fun setTitle(title: String) {
        activity?.toolbar_layout?.title = title
    }

    companion object {
        const val MOVIE_NAME_ARGUMENT_ID = "movie_name_argument_id"
    }
}
