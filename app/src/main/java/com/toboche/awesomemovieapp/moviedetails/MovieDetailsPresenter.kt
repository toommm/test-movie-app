package com.toboche.awesomemovieapp.moviedetails

import android.content.Context
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.base.Presenter
import com.toboche.awesomemovieapp.store.MoviesStoreApi

class MovieDetailsPresenter(
    private val moviesStore: MoviesStoreApi,
    private val title: String,
    private val context: Context
) : Presenter<MovieDetailsView>() {

    override fun attach(view: MovieDetailsView) {
        super.attach(view)
        val movieDetail = moviesStore.getMovieDetail(title)

        view.setTitle(movieDetail.name)
        view.setScore(context.getString(R.string.score, movieDetail.score.toString()))
        view.setDescription(movieDetail.description)
        view.setActors(movieDetail.actors.map {
            ActorListItem(
                it.name,
                context.getString(R.string.age, it.age.toString()),
                it.imageUrl
            )
        })
    }
}