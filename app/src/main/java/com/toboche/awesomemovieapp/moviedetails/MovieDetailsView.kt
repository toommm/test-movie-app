package com.toboche.awesomemovieapp.moviedetails

interface MovieDetailsView {
    fun setTitle(title: String)
    fun setScore(score: String)
    fun setDescription(description: String)
    fun setActors(actors: List<ActorListItem>)
}