package com.toboche.awesomemovieapp.movieslist

data class MovieListItem(
    val name: String,
    val lastUpdated: String,
    val onClick: () -> Unit
)