package com.toboche.awesomemovieapp.movieslist

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.moviedetails.MovieDetailActivity
import com.toboche.awesomemovieapp.moviedetails.MovieDetailFragment
import com.toboche.awesomemovieapp.store.MoviesStore
import kotlinx.android.synthetic.main.activity_movie_list.*
import kotlinx.android.synthetic.main.movie_list.*

class MovieActivity : AppCompatActivity(), MoviesListView {

    private var twoPane: Boolean = false
    private lateinit var moviesListPresenter: MoviesListPresenter
    private lateinit var moviesListRecyclerViewAdapter: MoviesListRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (movie_detail_container != null) {
            twoPane = true
        }

        setupRecyclerView(movie_list)
        moviesListPresenter = MoviesListPresenter(MoviesStore(), twoPane, this)
        moviesListPresenter.attach(this)
    }

    override fun openMovieDetailsInSecondPane(movieName: String) {
        val fragment = MovieDetailFragment().apply {
            arguments = Bundle().apply {
                putString(MovieDetailFragment.MOVIE_NAME_ARGUMENT_ID, movieName)
            }
        }
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.movie_detail_container, fragment)
            .commit()
    }

    override fun openMovieDetailsInNewActivity(movieName: String) {
        val intent = Intent(this, MovieDetailActivity::class.java).apply {
            putExtra(MovieDetailFragment.MOVIE_NAME_ARGUMENT_ID, movieName)
        }
        startActivity(intent)
    }

    override fun setMovies(movies: List<MovieListItem>) {
        moviesListRecyclerViewAdapter.setMovies(movies)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        moviesListRecyclerViewAdapter = MoviesListRecyclerViewAdapter()
        recyclerView.adapter = moviesListRecyclerViewAdapter
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            (recyclerView.layoutManager as LinearLayoutManager).orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)
    }

}
