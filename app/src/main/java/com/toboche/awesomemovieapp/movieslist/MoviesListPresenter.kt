package com.toboche.awesomemovieapp.movieslist

import android.content.Context
import android.text.format.DateUtils
import com.toboche.awesomemovieapp.R
import com.toboche.awesomemovieapp.base.Presenter
import com.toboche.awesomemovieapp.model.Movie
import com.toboche.awesomemovieapp.store.MoviesStoreApi

class MoviesListPresenter(
    val moviesStore: MoviesStoreApi,
    val twoPane: Boolean,
    val context: Context
) : Presenter<MoviesListView>() {
    override fun attach(view: MoviesListView) {
        super.attach(view)
        view.setMovies(
            moviesStore.getMovies()
                .map {
                    MovieListItem(
                        it.name,
                        context.resources.getString(
                            R.string.last_updated, DateUtils.formatElapsedTime(it.lastUpdated.toLong())
                        ),
                        onMovieListItemClick(this.view!!, it)
                    )
                }
        )
    }

    private fun onMovieListItemClick(
        view: MoviesListView,
        it: Movie
    ) = {
        if (twoPane) {
            view.openMovieDetailsInSecondPane(it.name)
        } else {
            view.openMovieDetailsInNewActivity(it.name)
        }
    }
}