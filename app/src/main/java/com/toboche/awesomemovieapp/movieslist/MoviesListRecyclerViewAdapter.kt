package com.toboche.awesomemovieapp.movieslist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.toboche.awesomemovieapp.R
import kotlinx.android.synthetic.main.movie_list_content.view.*

class MoviesListRecyclerViewAdapter :
    RecyclerView.Adapter<MoviesListRecyclerViewAdapter.MovieViewHolder>() {
    private var values: List<MovieListItem> = emptyList()

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as MovieListItem
            item.onClick()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_list_content, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holderMovie: MovieViewHolder, position: Int) {
        val item = values[position]
        holderMovie.name.text = item.name
        holderMovie.lastUpdated.text = item.lastUpdated

        with(holderMovie.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    fun setMovies(movies: List<MovieListItem>) {
        values = movies
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.id_name
        val lastUpdated: TextView = view.elapsed_time
    }
}