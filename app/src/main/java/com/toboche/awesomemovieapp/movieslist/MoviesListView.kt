package com.toboche.awesomemovieapp.movieslist

interface MoviesListView {
    fun setMovies(movies: List<MovieListItem>)
    fun openMovieDetailsInSecondPane(movieName: String)
    fun openMovieDetailsInNewActivity(movieName: String)
}