package com.toboche.awesomemovieapp.store

import com.toboche.awesomemovieapp.model.Movie
import com.toboche.awesomemovieapp.model.MovieDetail

class MoviesStore : MoviesStoreApi {

    external override fun getMovies(): Array<Movie>

    external override fun getMovieDetail(name: String): MovieDetail

    companion object {
        init {
            System.loadLibrary("movies-store")
        }
    }
}

