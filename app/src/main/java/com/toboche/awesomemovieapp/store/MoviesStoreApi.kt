package com.toboche.awesomemovieapp.store

import com.toboche.awesomemovieapp.model.Movie
import com.toboche.awesomemovieapp.model.MovieDetail

interface MoviesStoreApi {
    fun getMovies(): Array<Movie>
    fun getMovieDetail(name: String): MovieDetail
}