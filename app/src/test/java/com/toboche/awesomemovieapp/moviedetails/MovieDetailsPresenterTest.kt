package com.toboche.awesomemovieapp.moviedetails

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.toboche.awesomemovieapp.model.Actor
import com.toboche.awesomemovieapp.model.MovieDetail
import com.toboche.awesomemovieapp.store.MoviesStoreApi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class MovieDetailsPresenterTest {

    val mockView = mock<MovieDetailsView>()
    val mockMovieDetailName = "moveDetailName"
    val movieDetailDescription = "movieDetailDescription"
    val score = 12F
    val actorName = "actorName"
    val actorAge = 123
    val actorImageUrl = "someUrlHere"
    val actors = arrayOf(
        Actor(
            actorName,
            actorAge,
            actorImageUrl
        )
    )
    val mockMovieDetail = MovieDetail(
        mockMovieDetailName,
        score,
        actors,
        movieDetailDescription
    )
    val mockMovieNameFromParent = "mockMovieNameFromParent"
    val mockOfMoviesStore = mock<MoviesStoreApi> {
        on { getMovieDetail(mockMovieNameFromParent) }.thenReturn(mockMovieDetail)
    }
    val sytemUnderTest =
        MovieDetailsPresenter(mockOfMoviesStore, mockMovieNameFromParent, RuntimeEnvironment.application)

    val actorsArgumentCaptor = argumentCaptor<List<ActorListItem>>()
    @Test
    fun `should set title`() {
        sytemUnderTest.attach(mockView)

        verify(mockOfMoviesStore).getMovieDetail(mockMovieNameFromParent)

        verify(mockView).setTitle(mockMovieDetailName)
    }

    @Test
    fun `should set description`() {
        sytemUnderTest.attach(mockView)

        verify(mockView).setDescription(movieDetailDescription)
    }

    @Test
    fun `should set score`() {
        sytemUnderTest.attach(mockView)

        verify(mockView).setScore("Score: $score")
    }

    @Test
    fun `should set actors`() {
        sytemUnderTest.attach(mockView)

        verify(mockView).setActors(actorsArgumentCaptor.capture())
        assertThat(actorsArgumentCaptor.firstValue).hasSize(1)
        assertThat(actorsArgumentCaptor.firstValue[0].name).isEqualTo(actorName)
        assertThat(actorsArgumentCaptor.firstValue[0].age).isEqualTo("Age: $actorAge")
        assertThat(actorsArgumentCaptor.firstValue[0].imageUrl).isEqualTo(actorImageUrl)

    }
}