package com.toboche.awesomemovieapp.movieslist

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.toboche.awesomemovieapp.model.Movie
import com.toboche.awesomemovieapp.store.MoviesStoreApi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class MoviesListPresenterTest {
    val mockOfView = mock<MoviesListView>()
    val mockMovieName = "movieName"
    val mockMovies = arrayOf(
        Movie(
            mockMovieName,
            432
        )
    )
    val mockOfMoviesStore = mock<MoviesStoreApi> {
        on { getMovies() }.thenReturn(mockMovies)
    }
    var systemUnderTest = MoviesListPresenter(
        mockOfMoviesStore,
        false,
        RuntimeEnvironment.application
    )
    val movieListArgumentCaptor = argumentCaptor<List<MovieListItem>>()

    @Test
    fun `should set movies to show`() {
        systemUnderTest.attach(mockOfView)

        verify(mockOfView).setMovies(any())
    }

    @Test
    fun `should fill movie with texts`() {
        systemUnderTest.attach(mockOfView)

        verify(mockOfView).setMovies(movieListArgumentCaptor.capture())

        val movieSentToPresenter = movieListArgumentCaptor.firstValue[0]

        assertThat(movieSentToPresenter.name).isEqualTo(mockMovieName)
        assertThat(movieSentToPresenter.lastUpdated).isEqualTo("07:12")
    }

    @Test
    fun `should open move details in another activity when single pane`() {
        systemUnderTest.attach(mockOfView)

        invokeClickOnFirstMovieInView()

        verify(mockOfView).openMovieDetailsInNewActivity(mockMovieName)
    }

    @Test
    fun `should open move details in another pane when two pane`() {
        systemUnderTest = MoviesListPresenter(
            mockOfMoviesStore,
            true,
            RuntimeEnvironment.application
        )
        systemUnderTest.attach(mockOfView)

        invokeClickOnFirstMovieInView()

        verify(mockOfView).openMovieDetailsInSecondPane(mockMovieName)
    }

    private fun invokeClickOnFirstMovieInView() {
        verify(mockOfView).setMovies(movieListArgumentCaptor.capture())
        val moviesSetOnPresenter = movieListArgumentCaptor.firstValue
        moviesSetOnPresenter[0].onClick()
    }
}
